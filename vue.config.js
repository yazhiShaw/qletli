// vue.config.js
module.exports = {
  publicPath: './',
  chainWebpack: config => {
    const imagesRule = config.module.rule('images')
    imagesRule
      .use('image-webpack-loader')
      .loader('image-webpack-loader')
      .options({
        bypassOnDebug: true
      })
      .end()
  },
  pages: {
    index: {
      // 页面的入口文件
      entry: 'src/pages/index/main.js',
      // 页面的模板文件
      template: 'public/index.html',
      // build 生成的文件名称  例： dist/index.html
      filename: 'index.html',
      title: '阿达木单抗-首页',
      faviconPath: './logo.png'
    },
    // 格乐立大事件
    bigNews: 'src/pages/about/bigNews/main.js',
    // 产品说明书
    productDescritption: 'src/pages/about/productDescritption/main.js',
    // 用法用量指引
    usePage: 'src/pages/about/usePage/main.js',
    // 注射视频指引
    injectVideo: 'src/pages/about/injectVideo/main.js',
    // 康复百科
    article: 'src/pages/patient/article/main.js',
    // 名师讲堂
    lecture: 'src/pages/patient/lecture/main.js',
    // 指南推荐
    handbook: 'src/pages/doctor/handbook/main.js',
    // document
    document: 'src/pages/doctor/document/main.js',
    // 相关文献
    literature: 'src/pages/doctor/literature/main.js',
    // 联系我们
    contact: 'src/pages/contact/main.js',
  },
}

