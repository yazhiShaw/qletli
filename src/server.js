import axios from "axios";
let api = "https://gate.maiduote.com";
if (process.env.VUE_APP_TITLE === "production") {
  api = "https://gate.maiduote.com";
} else if (process.env.VUE_APP_TITLE === "test") {
  api = "https://gate.maiduote.com";
}


// 获取医生模块
export async function getDoctorList(query = {}) {
  const resp = await axios.get(`${api}/pub/dzone/list`, {
    params: query
  });
  return resp.data;
}
